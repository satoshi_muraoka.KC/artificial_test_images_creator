#!/bin/bash
DOCKER_DIR=`dirname ${0}`
source ${DOCKER_DIR}/settings.sh ${DOCKER_DIR}

CONTAINER_ID=`docker ps -aq -f name=${CONTAINER_NAME}`
if [ -n "${CONTAINER_ID}" ]; then
  ${DOCKER_DIR}/stop_container.sh
fi

echo "docker run ${RUN_OPTION}"
docker run ${RUN_OPTION}
