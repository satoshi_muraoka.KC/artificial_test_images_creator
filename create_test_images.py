#!/usr/bin/env python
import argparse
import copy
import cv2
import glob
import mmcv
import numpy as np
import os
import pathlib
import pycocotools.mask as mask_utils
import pytz
import random
import shutil
import subprocess

from datetime import datetime
# from mmdet.datasets.api_wrappers import COCO
from PIL import Image
from tqdm import tqdm


# 現在時刻を取得する。
tokyo_tz = pytz.timezone('Asia/Tokyo')
current_time = datetime.now(tokyo_tz)

# 'whoami' コマンドでユーザー名を取得する。
result = subprocess.run(['whoami'], stdout=subprocess.PIPE)
username = result.stdout.decode().strip()
SRC_DIR_PATH = f"/mnt/storage/data/{username}/cp2/artificial_cp_images"
SRC_LINK_DIR = "src_data"
OUTPUT_DIR_PATH = f"/mnt/storage/data/{username}/cp2/artificial_test_images"
OUTPUT_LINK_DIR = "output"

category_id = 1

class CategoriesTemplate(object):

    def __init__(self, id=1, name="cp", supercategory="cp"):
        self.id = id
        self.name = name
        self.supercategory = supercategory

    def to_dictionary(self):
        return {
            "id": self.id,
            "name": self.name,
            "supercategory": self.supercategory
        }


class ImagesTemplate(object):

    def __init__(self, id=None, file_name=None, height=None, width=None):
        self.id = id
        self.file_name = file_name
        self.height = height
        self.width = width

    def to_dictionary(self):
        return {
            "id": self.id,
            "file_name": self.file_name,
            "height": self.height,
            "width": self.width
        }


class InfoTemplate(object):

    def __init__(self, description=""):
        self.contributor = "Kyocera"
        self.description = description
        self.url = ""
        self.version = ""

        self.date_create = current_time.strftime('%Y/%m/%d %H:%M:%S')
        self.year = current_time.year

    def to_dictionary(self):
        return {
            "contributor": self.contributor,
            "date_create": self.date_create,
            "description": self.description,
            "url": self.url,
            "version": self.version,
            "year": self.year
        }


class LicensesTemplate(object):

    def __init__(self, id=1, name="test"):
        self.id = id
        self.name = name
        self.url = f"http://{name}"

    def to_dictionary(self):
        return {
            "id": self.id,
            "name": self.name,
            "url": self.url
        }


class FreeCp(object):
    """A free (not attached to any images) work."""

    object_count = 0

    def __init__(self, img, mask=None, areas=None):
        # アルファチャンネルがある場合はRGBとアルファチャンネルを分ける。
        if img.shape[2] == 4:
            self.img = np.array(img[:, :, :3], dtype=np.uint8)
            if mask is None:
                # アルファチャンネルからマスクを作成する。
                h, w, _ = self.img.shape
                self.mask = np.ones((h, w), dtype=np.uint8) * (img[:, :, 3]/255).astype(np.uint8)
            else:
                self.mask = mask
        elif img.shape[2] == 3:
            self.img = np.array(img, dtype=np.uint8)
            if mask is None:
                h, w, _ = self.img.shape
                self.mask = np.ones((h, w), dtype=np.uint8)
            else:
                self.mask = mask

        self.areas = areas

        FreeCp.object_count += 1


    @staticmethod
    def get_count():
        return FreeCp.object_count

    def random_rotate(self):
        """Randomly rotate work."""
        angle = np.random.randint(0, 360)
        self.rotate(angle)

    def rotate(self, angle):
        """Rotate work."""
        img = Image.fromarray(self.img)
        mask = Image.fromarray(self.mask)

        img = img.rotate(angle, resample=Image.BILINEAR, expand=True)
        mask = mask.rotate(angle, resample=Image.NEAREST, expand=True)

        self.img , self.mask = self.cut_unnecessary_pixels(np.asarray(img), np.asarray(mask))
        self.area = self.mask.sum()

    def cut_unnecessary_pixels(self, img, mask):
        contours, hierarchy = cv2.findContours(image=mask.astype(np.uint8) * 255, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_SIMPLE)
        if hierarchy is not None:
            outermost_max_area_index = None
            outermost_max_area = -1
            for i, current_hierarchy in enumerate(hierarchy[0]):
                if current_hierarchy[3] == -1:
                    area = cv2.contourArea(contour=contours[i])
                    if area > outermost_max_area:
                        outermost_max_area_index = i
                        outermost_max_area = area
        x, y, w, h = cv2.boundingRect(contours[outermost_max_area_index])
        img = img[y:y+h, x:x+w, :]
        mask = mask[y:y+h, x:x+w]
        return img, mask

    def padding(self, pixels=1):
        h, w, _ = self.img.shape
        padded_h = h + pixels * 2
        padded_w = w + pixels * 2

        padded_img = np.zeros((padded_h, padded_w, 3), dtype=np.uint8)
        padded_img[pixels : pixels + h, pixels : pixels + w, :] = self.img
        self.img = padded_img

        padded_mask = np.zeros((padded_h, padded_w), dtype=np.uint8)
        padded_mask[pixels : pixels + h, pixels : pixels + w] = self.mask
        self.mask = padded_mask


class ImageWithMask(object):
    """A image&mask format for random pasting."""

    def __init__(self, img):
        self.img = np.array(img, dtype=np.uint8)
        self.mask = np.zeros(self.img.shape[:2], dtype=np.uint8)
        self.areas = []
        self.object_ids = []

        self.bg_img = copy.deepcopy(self.img)
        self.bg_mask = np.zeros((self.bg_img.shape[:2])).astype(np.uint8)

    def paste(self, free_cp, top=None, left=None, top_min=None, top_max=None, left_min=None, left_max=None, top_cut_size=0, bottom_cut_size=0, left_cut_size=0, right_cut_size=0, misalignment_max=None, misalignment_top_enable=False):
        """Paste a free work.
        """

        h, w, _ = self.img.shape
        ih, iw, _ = free_cp.img.shape
        assert h >= ih and w >= iw, 'Instance larger than background'
        # print('top =', top,'left =', left,'top_min =', top_min,'top_max =', top_max,'left_min =', left_min,'left_max =', left_max)
        if top_min is None:
            top_min = 0
        if top_max is None:
            top_max = h - ih
        if left_min is None:
            left_min = 0
        if left_max is None:
            left_max = w - iw
        # print('top =', top,'left =', left,'top_min =', top_min,'top_max =', top_max,'left_min =', left_min,'left_max =', left_max)
        if top is None:
            top = np.random.randint(top_min, top_max)
        if left is None:
            left = np.random.randint(left_min, left_max)
        # paste image
        object_id = len(self.object_ids) + 1
        mask_for_image = np.copy(free_cp.mask)
        contours, hierarchy = cv2.findContours(mask_for_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        outside_contour = None
        mask_for_image = np.zeros((ih, iw), np.uint8)
        if hierarchy is not None:
            for i, current_hierarchy in enumerate(hierarchy[0]):
                if current_hierarchy[3] == -1:
                    outside_contour = contours[i]
                    mask_for_image = cv2.drawContours(mask_for_image, [outside_contour], 0, (1), -1)
        # cv2.imwrite('debug_mask.png', mask_for_image * 255)
        # print('mask_for_image.shape =', mask_for_image.shape)
        # print('free_cp.mask.shape =', free_cp.mask.shape)
        if misalignment_max is not None:
            mask_for_image_misalignment = np.copy(mask_for_image)
            misalignment = np.random.randint(0, misalignment_max)
            if misalignment_top_enable:
                misalignment_top = misalignment + top
                misalignment_bottom = misalignment + top + ih - top_cut_size - bottom_cut_size
                misalignment = np.random.randint(0, misalignment_max)
            else:
                misalignment_top = top
                misalignment_bottom = top + ih - top_cut_size - bottom_cut_size
            misalignment_left = misalignment + left
            misalignment_right = misalignment + left + iw - left_cut_size - right_cut_size
            if misalignment_bottom >= h:
                misalignment_bottom_cut_size = misalignment_bottom - h
                misalignment_bottom = h
            else:
                misalignment_bottom_cut_size = 0
            if misalignment_right >= w:
                misalignment_right_cut_size = misalignment_right - w
                misalignment_right = w
            else:
                misalignment_right_cut_size = 0
            # print('misalignment_bottom:', misalignment_bottom)
            # print('misalignment_right:', misalignment_right)
            # print('misalignment_bottom_cut_size:', misalignment_bottom_cut_size)
            # print('misalignment_right_cut_size:', misalignment_right_cut_size)
            misalignment_i_top = top_cut_size
            misalignment_i_bottom = ih - bottom_cut_size - misalignment_bottom_cut_size
            misalignment_i_left = left_cut_size
            misalignment_i_right = iw - right_cut_size - misalignment_right_cut_size
            mask_for_image_misalignment = mask_for_image_misalignment[misalignment_i_top:misalignment_i_bottom, misalignment_i_left:misalignment_i_right]
            self.img[misalignment_top:misalignment_bottom, misalignment_left:misalignment_right] = (
                self.img[misalignment_top:misalignment_bottom, misalignment_left:misalignment_right] *
                (1 - mask_for_image_misalignment[..., np.newaxis]) +
                free_cp.img[misalignment_i_top:misalignment_i_bottom, misalignment_i_left:misalignment_i_right] * mask_for_image_misalignment[..., np.newaxis])
        mask_for_image = mask_for_image[top_cut_size:ih - bottom_cut_size, left_cut_size:iw - right_cut_size]
        self.img[top:top + ih - top_cut_size - bottom_cut_size, left:left + iw - left_cut_size - right_cut_size] = (
            self.img[top:top + ih - top_cut_size - bottom_cut_size, left:left + iw - left_cut_size - right_cut_size] *
            (1 - mask_for_image[..., np.newaxis]) +
            free_cp.img[top_cut_size:ih - bottom_cut_size, left_cut_size:iw - right_cut_size] * mask_for_image[..., np.newaxis])
        self.mask[top:top + ih - top_cut_size - bottom_cut_size, left:left + iw - left_cut_size - right_cut_size] = (
            self.mask[top:top + ih - top_cut_size - bottom_cut_size, left:left + iw - left_cut_size - right_cut_size] *
            (1 - free_cp.mask[top_cut_size:ih - bottom_cut_size, left_cut_size:iw - right_cut_size]) + free_cp.mask[top_cut_size:ih - bottom_cut_size, left_cut_size:iw - right_cut_size] * object_id)

        self.object_ids.append(object_id)
        self.areas.append(free_cp.area)
        return top, left


    def create_sides_of_object(self, scale_num=5, step=20, brightness_shift=0):

        new_bg_img = copy.deepcopy(self.bg_img)
        new_bg_mask = copy.deepcopy(self.bg_mask)

        # 輝度の減少量を定義する。
        brightness_matrix = np.ones(self.img.shape, dtype='uint8') * brightness_shift

        # for i in range(scale_num, -1, -1):
        for i in range(scale_num, 0, -1):
            # 縮小後の次元を計算する。
            new_width = int(self.img.shape[1] - i * step)
            new_height = int(self.img.shape[0] - i * step)

            # rgb画像とmaskをリサイズする。
            small_img = cv2.resize(self.img, (new_width, new_height), interpolation = cv2.INTER_NEAREST)
            small_mask = cv2.resize(self.mask, (new_width, new_height), interpolation = cv2.INTER_NEAREST)

            # 縮小画像を貼り付けるための元の背景画像およびマスクのコピーを用意する。
            scaled_img = copy.deepcopy(self.bg_img)
            scaled_mask = copy.deepcopy(self.bg_mask)

            # 縮小画像を貼り付ける topleft 座標を計算する。
            left = int((scaled_img.shape[1] - new_width) / 2)
            top = int((scaled_img.shape[0] - new_height) / 2)

            # 縮小画像を元の背景画像に貼り付けることにより、縮小前の次元に合わせる。
            scaled_img[top:top+new_height, left:left+new_width] = small_img
            scaled_mask[top:top+new_height, left:left+new_width] = small_mask

            if brightness_shift:
                # 受け取った画像から作成したスカラー値を引く
                scaled_img = cv2.subtract(scaled_img, brightness_matrix)

            # 縮小画像のマスクをもとにオブジェクトを取り出して背景画像に貼り付ける。
            for object_id in self.object_ids:
                new_bg_img[scaled_mask == object_id] = scaled_img[scaled_mask == object_id]

            # マスクを合成する。
            new_bg_mask = np.where(scaled_mask == 0, new_bg_mask, scaled_mask)


        # 縮小画像のマスクをもとにオブジェクトを取り出して背景画像に貼り付ける。
        for object_id in self.object_ids:
            new_bg_img[self.mask == object_id] = self.img[self.mask == object_id]

        # マスクを合成する。
        new_bg_mask = np.where(self.mask == 0, new_bg_mask, self.mask)

        self.img = new_bg_img
        self.mask = new_bg_mask
        areas = []
        for object_id in self.object_ids:
            area = (self.mask == object_id).sum()
            areas.append(area)

        self.areas = areas


    def smoothing_image(self, kernel_size=3, type="blur"):
        '''Smooth the image using the specified method.
        '''
        filter_size = (kernel_size, kernel_size)

        if type=="blur":
            # 平均化フィルターを適用
            self.img = cv2.blur(self.img, filter_size)
        elif type=="GaussianBlur":
            # ガウシアンフィルターを適用
            self.img = cv2.GaussianBlur(self.img, filter_size, 0)
        elif type=="medianBlur":
            # メディアンフィルターを適用
            self.img = cv2.medianBlur(self.img, kernel_size)
        elif type=="bilateral":
            # バイラテラルフィルターを適用
            self.img = cv2.bilateralFilter(self.img, 9, 75, 75)


    def iter_instances(self):
        """Iterate through all instances of an image.

        Yields:
            dict: An instance in COCO 'annotations' format.
        """
        mask = self.mask
        h, w = mask.shape
        for i, (object_id,
                full_area) in enumerate(zip(self.object_ids, self.areas)):
            object_id = i + 1
            instance_mask = np.asfortranarray(mask == object_id)
            rle = mask_utils.encode(instance_mask)
            rle['counts'] = rle['counts'].decode()
            bbox = mask_utils.toBbox(rle).tolist()
            area = float(mask_utils.area(rle))
            if area >= 1:
                hidden_percentage = 1 - area / full_area
                assert 0.0 <= hidden_percentage <= 1.0
                yield dict(
                    # category_id=object_id,
                    category_id=category_id,
                    bbox=bbox,
                    area=area,
                    hidden_percentage=hidden_percentage,
                    segmentation=rle,
                    height=h,
                    width=w,
                    iscrowd=0)


def dump_instances(samples, out_dir):
    """Dump instances from samples.

    Args:
        samples (ImageWithAnnotation)
        out_dir (str): A directory to save instances.
    Returns:
        list[str]: A list of paths of instances.
    """
    paths = []
    instance_id = 0
    for sample in samples:
        for instance in sample.iter_instances():
            path = os.path.join(out_dir, f'instance_{instance_id}.pkl')
            mmcv.dump(instance, path)
            instance_id += 1
            paths.append(path)

    return paths


def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def parse_args():
    parser = argparse.ArgumentParser(
        description='Create test images')
    parser.add_argument('background', type=str, help='background image')
    parser.add_argument('delete_out_dir', type=str2bool, default=False, help='Set to True to delete the existing output directory.')
    parser.add_argument('--seed', type=int, default=0)
    parser.add_argument('--rotate-step', type=int, default=10)
    parser.add_argument('--one-place-repeate-num', type=int, default=1, help='place mode')

    return parser.parse_args()


def create_output_link(args):

    if args.delete_out_dir:
        # 既存ディレクトリを削除して新規作成
        if os.path.exists(OUTPUT_DIR_PATH):
            shutil.rmtree(OUTPUT_DIR_PATH)
        os.makedirs(OUTPUT_DIR_PATH)

    # シンボリックリンクの存在を確認し、存在しない場合は作成
    if not os.path.exists(OUTPUT_LINK_DIR):
        pathlib.Path(OUTPUT_LINK_DIR).symlink_to(OUTPUT_DIR_PATH)


def main():
    args = parse_args()

    # ソースディレクトリの存在を確認し、存在しない場合は処理を終了する
    if not os.path.exists(SRC_DIR_PATH):
        print(f"{SRC_DIR_PATH} が存在しません。")
        exit()
    # ソースのシンボリックリンクの存在を確認し、存在しない場合は作成
    if not os.path.exists(SRC_LINK_DIR):
        pathlib.Path(SRC_LINK_DIR).symlink_to(SRC_DIR_PATH)

    create_output_link(args)

    target_directories = sorted(os.listdir(SRC_LINK_DIR))
    # target_directories = ["QFN_type02"]
    print(f"target_directories: {target_directories}")

    bg_image = cv2.imread(args.background)
    # dummy_bg_image = np.ones(bg_image.shape).astype(np.uint8)*255
    dummy_bg_image = np.ones(bg_image.shape).astype(np.uint8)

    for target in target_directories:
        # set random seed
        random.seed(args.seed)
        np.random.seed(args.seed)
        # Make new annotation
        coco_anns = []
        coco_imgs = []
        img_id = 0
        ann_id = 0

        target_dir = os.path.join(SRC_LINK_DIR, target)
        cp_file_path_list = sorted(glob.glob(os.path.join(target_dir, "*.png")))

        out_dir = os.path.join(OUTPUT_LINK_DIR, target)

        # 出力ディレクトリが既存かつ削除フラグが True の場合、出力ディレクトリを削除する。
        # 出力ディレクトリが既存かつ削除フラグが False の場合、処理をスキップする。
        if os.path.exists(out_dir):
            if args.delete_out_dir:
                shutil.rmtree(out_dir)
            else:
                continue

        # Generate with random pasting
        max_rotate = 360
        total_loop_num = len(cp_file_path_list)
        bar = tqdm(total=total_loop_num)
        bar.set_description(f'{target} オブジェクトx9')
        for i, work_path in enumerate(cp_file_path_list):
            angle = (i * args.rotate_step) % max_rotate
            bar.update(1)
            # ワーク画像を読み込む。
            work = FreeCp(img=cv2.imread(work_path, cv2.IMREAD_UNCHANGED))
            # ワーク画像を回転する。
            work.rotate(angle)
            # ワーク画像をパディングする。ワークを並べる際にすき間を開けるため。
            work.padding(pixels=4)

            # オブジェクト単体のサイズと、縦横に３つずつ並べた時の集団のサイズを取得する。
            work_h, work_w, _ = work.img.shape
            mass_h = work_h * 3
            mass_w = work_w * 3

            # 合成画像を操作するインスタンスを生成する。
            image_with_mask = ImageWithMask(img=np.copy(dummy_bg_image))

            h, w, _ = image_with_mask.img.shape
            if mass_h > h:
                print('mass_h =', mass_h, '> h =', h)
                continue
            if mass_w > w:
                print('mass_w =', mass_w, '> h =', w)
                continue

            # 0-9番目のオブジェクトの座標を決める。
            mass_top = np.random.randint(0, h - mass_h)
            mass_left = np.random.randint(0, w - mass_w)
            object_top_left = [
                (mass_top, mass_left),                    # top left
                (mass_top, mass_left+work_w),             # top center
                (mass_top, mass_left+work_w*2),           # top right
                (mass_top+work_h, mass_left),             # center left
                (mass_top+work_h, mass_left+work_w),      # center
                (mass_top+work_h, mass_left+work_w*2),    # center right
                (mass_top+work_h*2, mass_left),           # bottom left
                (mass_top+work_h*2, mass_left+work_w),    # bottom center
                (mass_top+work_h*2, mass_left+work_w*2),  # bottom right
            ]

            for top, left in object_top_left:
                # 背景にワークをランダムに貼り付ける。
                image_with_mask.paste(work, top=top, left=left)
            # 側面の映り込みを再現する。
            image_with_mask.create_sides_of_object(scale_num=26, step=2, brightness_shift=75)

            for coco_ann in image_with_mask.iter_instances():
                coco_ann['image_id'] = img_id
                coco_ann['id'] = ann_id
                coco_anns.append(coco_ann)
                ann_id += 1

            coco_img = ImagesTemplate()
            coco_img.id = i
            filename = os.path.basename(work_path).split('.')[0]
            coco_img.file_name = f'{filename}_{"{:05d}".format(img_id)}_color.png'
            coco_img.height = h
            coco_img.width = w
            coco_imgs.append(coco_img.to_dictionary())

            # 背景を差し替える。
            mask_3d = np.stack((image_with_mask.mask,)*3, axis=-1)
            image_with_mask.img = np.where(mask_3d != 0, image_with_mask.img, bg_image)

            # 画像を平滑化する。
            image_with_mask.smoothing_image(kernel_size=5, type="blur")

            mmcv.imwrite(image_with_mask.img, os.path.join(out_dir, coco_img.file_name))

            # # debug
            # file_name_mask = coco_img.file_name.replace("color", "mask")
            # mmcv.imwrite(image_with_mask.mask * 255, os.path.join(out_dir, file_name_mask))

            img_id += 1


        # # Generate with random pasting
        # max_rotate = 360
        # total_loop_num = len(cp_file_path_list)
        # bar = tqdm(total=total_loop_num)
        # bar.set_description('1インスタンス')
        # for repeat_num in range(args.one_place_repeate_num):
        #     for i, work_path in enumerate(cp_file_path_list):
        #         angle = (i * args.rotate_step) % max_rotate
        #         bar.update(1)
        #         # ワーク画像を読み込んで回転する。
        #         work = FreeCp(img=cv2.imread(work_path))
        #         work.rotate(angle)

        #         # 合成画像を操作するインスタンスを生成する。
        #         image_with_mask = ImageWithMask(img=np.copy(bg_image))

        #         h, w, _ = image_with_mask.img.shape
        #         ih, iw, _ = work.img.shape
        #         if ih > h:
        #             print('ih =', ih, '> h =', h)
        #             continue
        #         if iw > w:
        #             print('iw =', iw, '> h =', w)
        #             continue

        #         # 背景にワークをランダムに貼り付ける。
        #         image_with_mask.paste(work)
        #         # 側面の映り込みを再現する。
        #         image_with_mask.create_sides_of_object(scale_num=26, step=2)
        #         # 画像を平滑化する。
        #         image_with_mask.smoothing_image(kernel_size=3, type="blur")

        #         for coco_ann in image_with_mask.iter_instances():
        #             coco_ann['image_id'] = img_id
        #             coco_ann['id'] = ann_id
        #             coco_anns.append(coco_ann)
        #             ann_id += 1


        #         coco_img = ImagesTemplate()
        #         coco_img.id = i
        #         filename = os.path.basename(work_path)
        #         work_type = filename.split('_')[0]
        #         coco_img.file_name = f'test_{"{:05d}".format(img_id)}_{work_type}_color.png'
        #         coco_img.height = h
        #         coco_img.width = w
        #         coco_imgs.append(coco_img.to_dictionary())

        #         mmcv.imwrite(image_with_mask.img, os.path.join(out_dir, coco_img.file_name))

        #         # # debug
        #         # file_name_mask = f'test_{"{:05d}".format(img_id)}_{work_type}_mask.png'
        #         # mmcv.imwrite(image_with_mask.mask * 255, os.path.join(out_dir, file_name_mask))

        #         img_id += 1

        # save annotations.json
        coco_json = {
            "annotations": [],
            "categories": [],
            "images": [],
            "info": [],
            "licenses": [],
            "templates": []
        }
        coco_json['annotations'] = coco_anns
        coco_categories = CategoriesTemplate(id=category_id)
        coco_json['categories'].append(coco_categories.to_dictionary())
        coco_json['images'] = coco_imgs
        coco_info = InfoTemplate()
        coco_json['info'].append(coco_info.to_dictionary())
        coco_licenses = LicensesTemplate()
        coco_json['licenses'].append(coco_licenses.to_dictionary())
        coco_json['templates'] = []

        mmcv.dump(coco_json, os.path.join(out_dir, 'annotations.json'))


if __name__ == '__main__':
    main()
