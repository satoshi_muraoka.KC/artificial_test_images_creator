#!/bin/bash

# 背景画像を指定する。
background="bg_image/bg_001.png"

# 出力ディレクトリが既存の場合の処理フラグ。
delete_out_dir=0  # 0:False(削除しない), 1:True(削除する)

cmd="python create_test_images.py\
 ${background}\
 ${delete_out_dir}\
"
echo ${cmd}
eval ${cmd}
