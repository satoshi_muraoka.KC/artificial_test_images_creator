#!/usr/bin/env python
import argparse
import cv2
import numpy as np
import os
import pathlib
import shutil
import subprocess

from pycocotools.coco import COCO
from tqdm import tqdm


dir_name = "QFJ_type01"

color = [
    (0,0,255),    # 赤色
    (0,255,0),    # 緑色
    (255,0,0),    # 青色
    (255,0,255),  # マゼンタ
    (0,255,255),  # シアン
    (255,255,0),  # 黄色
    (128,0,0),    # 濃い青色
    (0,128,0),    # 濃い緑色
    (0,0,128)     # 濃い赤色
]

# 'whoami' コマンドでユーザー名を取得する。
result = subprocess.run(['whoami'], stdout=subprocess.PIPE)
username = result.stdout.decode().strip()
SRC_LINK_DIR = "output"
OUTPUT_DIR_PATH = f"/mnt/storage/data/{username}/cp2/artificial_test_images"
OUTPUT_LINK_DIR = "output"

def parse_args():
    parser = argparse.ArgumentParser(
        description='Confirm annotations')
    parser.add_argument('dir_name', type=str, help='target directory name')

    return parser.parse_args()


def create_output_link():
    # シンボリックリンクの存在を確認し、存在しない場合は作成
    if not os.path.exists(OUTPUT_LINK_DIR):
        pathlib.Path(OUTPUT_LINK_DIR).symlink_to(OUTPUT_DIR_PATH)


def main():
    args = parse_args()
    create_output_link()

    # args.dir_name がパスの場合とディレクトリ名の場合で save_dir を分ける。
    if os.path.dirname(args.dir_name):
        save_dir = os.path.join(args.dir_name, "confirm")
        annotation_file = os.path.join(args.dir_name, "annotations.json")
    else:
        save_dir = os.path.join(OUTPUT_LINK_DIR, "confirm", args.dir_name)
        annotation_file = os.path.join(SRC_LINK_DIR, args.dir_name, "annotations.json")

    # 既存ディレクトリを削除して新規作成
    if os.path.exists(save_dir):
        shutil.rmtree(save_dir)
    os.makedirs(save_dir)

    coco_api_instance = COCO(annotation_file)

    # アノテーションが存在する画像IDを取得する。
    img_ids = coco_api_instance.getImgIds()

    bar = tqdm(total=len(img_ids), desc=f"{args.dir_name}")
    for img_id in img_ids:
        bar.update()
        # データを取り込む。
        img_detail = coco_api_instance.loadImgs(img_id)[0]
        # print(f"img_detail: {img_detail}")

        # 画像データを読み込む。
        img = cv2.imread(os.path.join(SRC_LINK_DIR, args.dir_name, img_detail["file_name"]))

        # 対応するアノテーションIDを取得する。
        ann_ids = coco_api_instance.getAnnIds(imgIds=img_detail["id"])

        # 全アノテーションを読み込む。
        annotations = coco_api_instance.loadAnns(ann_ids)
        # print(f"ann_ids: {ann_ids}")
        # print(f"annotations: {annotations}")


        # 各アノテーションを描画する。
        for n, ann in enumerate(annotations):
            n = n % len(color)

            # バウンディングボックスを描画する。
            box_left, box_top, box_w, box_h = list(map(int, ann["bbox"]))
            cv2.rectangle(img, (box_left, box_top), (box_left + box_w, box_top + box_h), color[n], 1)

            # マスクを描画する。
            mask = coco_api_instance.annToMask(ann)
            mask = np.stack([mask*color[n][0], mask*color[n][1], mask*color[n][2]], axis=2)
            img = cv2.addWeighted(img, 1, mask, 0.5, 0)  # マスクオーバーレイ

        cv2.imwrite(os.path.join(save_dir, f"conf_{img_detail['file_name']}"), img)
        # print(os.path.join(SRC_LINK_DIR, args.dir_name, img_detail["file_name"]))
        # input()



if __name__ == '__main__':
    main()
